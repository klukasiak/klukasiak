import org.junit.Assert;
import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MainTests {

    @Test
    public void testShowStudentByIdShouldReturnFirstStudent() throws SQLException {
        final String DB_URL = "jdbc:hsqldb:hsql://localhost/workdb";
        Connection conn = DriverManager.getConnection(DB_URL);
    }

    @Test
    public void testInsertStudent() throws SQLException {
        //given
        final String DB_URL = "jdbc:hsqldb:hsql://localhost/workdb";
        Connection conn = DriverManager.getConnection(DB_URL);
        Student s = new Student(9, "Zenon", 27);

        //when
        Student s1 = Main.insert(s, conn);
        //then
        Assert.assertEquals(s, s1);
    }

    @Test
    public void testShowStudentByID() throws SQLException {
        final String DB_URL = "jdbc:hsqldb:hsql://localhost/workdb";
        Connection conn = DriverManager.getConnection(DB_URL);
        Student s = new Student(1, "Adam", 20);

        Student s1 = Main.showStudentById(1, conn);

        Assert.assertEquals(s, s1);
    }


}
