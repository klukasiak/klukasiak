import java.sql.*;

public class Main {
    public static void main(String[] args) throws SQLException {
        final String DB_URL = "jdbc:hsqldb:hsql://localhost/workdb";

        Connection connection = DriverManager.getConnection(DB_URL);

        showStudents(connection);
        System.out.println();
        delete(5, connection);
        System.out.println("Usunieto 5");
        showStudents(connection);
        System.out.println();
        insert(new Student(5, "Mariusz", 21) , connection);
        showStudents(connection);
        System.out.println();
        System.out.println("Zmieniamy 5");
        update(5, 5, "Damian", 22, connection);
        showStudents(connection);
        System.out.println();
        showStudentById(1, connection);

    }

    public static Student insert(Student s, Connection connection) {
        try{
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO \"PUBLIC\".\"STUDENCI\"\n" +
                    "( \"ID\", \"NAME\", \"AGE\" )\n" +
                    "VALUES (?,?,?)");
            stmt.setInt(1, s.getId());
            stmt.setString(2, s.getName());
            stmt.setInt(3, s.getAge());
            stmt.executeUpdate();
            return s;
        } catch(SQLException e){
            return null;
        }
    }

    public static void delete(int id, Connection connection) throws SQLException {
        PreparedStatement stmt = connection.prepareStatement("DELETE FROM \"PUBLIC\".\"STUDENCI\" WHERE ID = ?");
        stmt.setInt(1, id);
        stmt.executeUpdate();
    }

    public static void update(int idToChange, int id, String name, int age, Connection connection) throws SQLException {
        PreparedStatement stmt = connection.prepareStatement("UPDATE \"PUBLIC\".\"STUDENCI\" SET ID = ?, NAME = ?, AGE = ? WHERE ID = ?");
        stmt.setInt(1, id);
        stmt.setString(2, name);
        stmt.setInt(3, age);
        stmt.setInt(4, idToChange);
        stmt.executeUpdate();
    }

    public static void showStudents(Connection connection) throws SQLException {
        String select = "SELECT * FROM \"PUBLIC\".\"STUDENCI\"";
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(select);
        while(rs.next()){
            int id = rs.getInt("ID");
            String name = rs.getString("NAME");
            int age = rs.getInt("AGE");
            System.out.println(id + " " + name + " " + age);
        }
    }

    public static Student showStudentById(int id, Connection connection) throws SQLException {
        try {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM \"PUBLIC\".\"STUDENCI\" WHERE ID = ?");
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            rs.next();
            Student s = new Student(rs.getInt("ID"), rs.getString("NAME"), rs.getInt("AGE"));
            return s;
        } catch (SQLException e){
            return null;
        }
    }
}
