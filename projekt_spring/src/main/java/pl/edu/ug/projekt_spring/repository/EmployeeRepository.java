package pl.edu.ug.projekt_spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.edu.ug.projekt_spring.domain.Employee;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {

    @Query(value = "SELECT e FROM Employee e ORDER BY e.lastName")
    List<Employee> findAllOrderByLastName();

    @Query(value = "SELECT count(e) FROM Employee e")
    int getCount();

    @Query(value = "SELECT e FROM Employee e WHERE e.lastName = :lastName")
    Optional<Employee> findByLastName(@Param("lastName") String lastName);

    @Query(value = "SELECT e FROM Employee e WHERE age < :age")
    List<Employee> findByAgeLessThan(@Param("age") int age);

    List<Employee> findByAgeGreaterThan(int age);

    List<Employee> findByDoeAfter(Date date);

    List<Employee> findAllByOrderByFirstNameAsc();

    Optional<Employee> findTopByOrderByAgeDesc();
}
