package pl.edu.ug.projekt_spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.edu.ug.projekt_spring.domain.PositionEmployee;

@Repository
public interface PositionEmployeeRepository extends JpaRepository<PositionEmployee, Long> {
}
