package pl.edu.ug.projekt_spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.ug.projekt_spring.domain.Position;
import pl.edu.ug.projekt_spring.repository.PositionRepository;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class PositionService {

    @Autowired
    private PositionRepository positionRepository;

    public Position addPosition(Position position) {
        return positionRepository.save(position);
    }

    public List<Position> getAllPositions() {
        return positionRepository.findAll();
    }

    public Position getPositionById(Long id) {
        return positionRepository.findById(id).orElse(null);
    }

    public Position updatePosition(Long id, Position position) {
        position.setId(id);
        return positionRepository.save(position);
    }

    @Transactional
    public Position deletePosition(Position position) {
        positionRepository.delete(position);
        return position;
    }
}
