package pl.edu.ug.projekt_spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.ug.projekt_spring.domain.PositionEmployee;
import pl.edu.ug.projekt_spring.repository.PositionEmployeeRepository;

import java.util.List;

@Service
public class PositionEmployeeService {

    @Autowired
    private PositionEmployeeRepository positionEmployeeRepository;

    public PositionEmployee addPositionEmployee(PositionEmployee employee) {
        return positionEmployeeRepository.save(employee);
    }

    public List<PositionEmployee> getAllPositionsEmployees() {
        return positionEmployeeRepository.findAll();
    }

    public PositionEmployee getPositionEmployeeById(Long id) {
        return positionEmployeeRepository.findById(id).orElse(null);
    }

    public PositionEmployee deletePositionEmployee(PositionEmployee positionEmployee) {
        positionEmployeeRepository.delete(positionEmployee);
        return positionEmployee;
    }
}
