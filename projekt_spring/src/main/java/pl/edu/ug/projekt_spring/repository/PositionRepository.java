package pl.edu.ug.projekt_spring.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.edu.ug.projekt_spring.domain.Position;

import javax.transaction.Transactional;

@Repository
public interface PositionRepository extends JpaRepository<Position, Long> {
    @Transactional
    @Override
    void delete(Position position);
}
