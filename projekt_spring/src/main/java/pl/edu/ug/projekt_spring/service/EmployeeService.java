package pl.edu.ug.projekt_spring.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.ug.projekt_spring.domain.Employee;
import pl.edu.ug.projekt_spring.repository.EmployeeRepository;

import java.sql.Date;
import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    private EmployeeRepository employeeRepository;

    public Employee addEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    public Employee getEmployeeById(Long id) {
        return employeeRepository.findById(id).orElse(null);
    }

    public Employee deleteEmployee(Employee employee) {
        employeeRepository.delete(employee);
        return employee;
    }

    public Employee updateEmployee(Long id, Employee employee) {
        employee.setId(id);
        return employeeRepository.save(employee);
    }

    public List<Employee> getAllEmployeesOrderedByLastName() {
        return employeeRepository.findAllOrderByLastName();
    }

    public int getCountEmployees() {
        return employeeRepository.getCount();
    }

    public Employee getEmployeeByLastName(String lastName) {
        return employeeRepository.findByLastName(lastName).orElse(null);
    }

    public List<Employee> getEmployeesYoungerThan(int age) {
        return employeeRepository.findByAgeLessThan(age);
    }

    public List<Employee> getEmployeesOlderThan(int age) {
        return employeeRepository.findByAgeGreaterThan(age);
    }

    public List<Employee> getEmployeesEmployeedAfter(Date date) {
        return employeeRepository.findByDoeAfter(date);
    }

    public List<Employee> getEmployeesOrderedByFirstName() {
        return employeeRepository.findAllByOrderByFirstNameAsc();
    }

    public Employee findOldestEmployee() {
        return employeeRepository.findTopByOrderByAgeDesc().orElse(null);
    }
}
