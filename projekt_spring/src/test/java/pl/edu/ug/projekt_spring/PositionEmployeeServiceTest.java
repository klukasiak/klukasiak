package pl.edu.ug.projekt_spring;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import pl.edu.ug.projekt_spring.domain.Employee;
import pl.edu.ug.projekt_spring.domain.Position;
import pl.edu.ug.projekt_spring.domain.PositionEmployee;
import pl.edu.ug.projekt_spring.service.EmployeeService;
import pl.edu.ug.projekt_spring.service.PositionEmployeeService;
import pl.edu.ug.projekt_spring.service.PositionService;

import java.sql.Date;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class PositionEmployeeServiceTest {

    @Autowired
    PositionEmployeeService positionEmployeeService;

    @Autowired
    PositionService positionService;

    @Autowired
    EmployeeService employeeService;

    Position position = new Position(null, "A", "B", 222, new ArrayList<>());
    Position position2 = new Position(null, "Bcc", "Cdddd", 3332, new ArrayList<>());
    Employee employee = new Employee(null, "A", "B", Date.valueOf("1992-02-22"), 27, new ArrayList<>());
    Employee employee2 = new Employee(null, "Abc", "Bbc", Date.valueOf("1997-02-12"), 22, new ArrayList<>());


    @BeforeEach
    void setUp() {
        positionService.addPosition(position);
        positionService.addPosition(position2);
        employeeService.addEmployee(employee);
        employeeService.addEmployee(employee2);
        PositionEmployee positionService = new PositionEmployee(null, employee, position);
        positionEmployeeService.addPositionEmployee(positionService);
        positionService = new PositionEmployee(null, employee2, position2);
        positionEmployeeService.addPositionEmployee(positionService);
    }

    @Test
    void shouldReturnAdded() {
        PositionEmployee pe = new PositionEmployee(null, employee, position2);
        assertEquals(pe, positionEmployeeService.addPositionEmployee(pe));
    }

    @Test
    void shouldReturnAllPositions() {
        assertEquals(2, positionEmployeeService.getAllPositionsEmployees().size());
    }

    @Test
    void shouldReturnById() {
        PositionEmployee pe = new PositionEmployee(null, employee, position2);
        Long id = positionEmployeeService.addPositionEmployee(pe).getId();
        assertEquals(pe, positionEmployeeService.getPositionEmployeeById(id));
    }

    @Test
    void shouldReturnDeleted() {
        PositionEmployee pe = new PositionEmployee(null, employee, position2);
        assertEquals(pe, positionEmployeeService.deletePositionEmployee(pe));
    }
}
