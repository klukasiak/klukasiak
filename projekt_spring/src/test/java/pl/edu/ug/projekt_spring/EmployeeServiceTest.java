package pl.edu.ug.projekt_spring;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import pl.edu.ug.projekt_spring.domain.Employee;
import pl.edu.ug.projekt_spring.service.EmployeeService;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class EmployeeServiceTest {

    @Autowired
    EmployeeService employeeService;

    @BeforeEach
    void setUp() {
        Employee employee = new Employee(null, "A", "B", Date.valueOf("1992-02-22"), 27, new ArrayList<>());
        employeeService.addEmployee(employee);
        employee = new Employee(null, "Abc", "Bbc", Date.valueOf("1997-02-12"), 22, new ArrayList<>());
        employeeService.addEmployee(employee);
    }

    @Test
    void addEmployeeShouldReturnThisEmployee() {
        Employee employee = new Employee(null, "A", "B", Date.valueOf("1992-02-22"), 22, new ArrayList<>());
        assertEquals(employee, employeeService.addEmployee(employee));
    }

    @Test
    void getAllShouldReturnTwo() {
        List<Employee> employeeList = employeeService.getAllEmployees();
        assertEquals(2, employeeList.size());
    }

    @Test
    void getByIdShouldReturnGiven() {
        Employee employee = new Employee(1L, "A", "B", Date.valueOf("1992-02-22"), 22, new ArrayList<>());
        employeeService.addEmployee(employee);
        Employee actual = employeeService.getEmployeeById(1L);
        assertEquals(employee, actual);
    }

    @Test
    void shouldReturnDeleted() {
        Employee employee = new Employee(1L, "A", "B", Date.valueOf("1992-02-22"), 22, new ArrayList<>());
        employeeService.addEmployee(employee);
        assertEquals(employee, employeeService.deleteEmployee(employee));
    }

    @Test
    void shouldReturnUpdated() {
        assertEquals(new Employee(), employeeService.updateEmployee(1L, new Employee()));
    }

    @Test
    void shouldReturnSortedListByLastName() {
        Employee employee = new Employee(null, "A", "Aaaa", Date.valueOf("1992-02-22"), 27, new ArrayList<>());
        employeeService.addEmployee(employee);
        List<Employee> expected = employeeService.getAllEmployees();
        expected.sort(Comparator.comparing(Employee::getLastName));
        List<Employee> actual = employeeService.getAllEmployeesOrderedByLastName();
        assertArrayEquals(expected.toArray(), actual.toArray());
    }

    @Test
    void countShouldReturnTwo() {
        assertEquals(2, employeeService.getCountEmployees());
    }

    @Test
    void shouldReturnFindedByLastName() {
        Employee employee = new Employee(null, "A", "Abcdefg", Date.valueOf("1992-02-22"), 27, new ArrayList<>());
        employeeService.addEmployee(employee);
        assertEquals(employee, employeeService.getEmployeeByLastName("Abcdefg"));
    }

    @Test
    void shouldReturnEmployeeYoungerThan() {
        List<Employee> employees = employeeService.getEmployeesYoungerThan(25);
        assertEquals(1, employees.size());
    }

    @Test
    void shouldReturnEmployeeOlderThan() {
        List<Employee> employees = employeeService.getEmployeesOlderThan(25);
        assertEquals(1, employees.size());
    }

    @Test
    void shouldReturnEmployeedAfter() {
        List<Employee> employees = employeeService.getEmployeesEmployeedAfter(Date.valueOf("1993-02-22"));
        assertEquals(1, employees.size());
    }

    @Test
    void shouldReturnOldest() {
        Employee employee = new Employee(null, "A", "Aaaa", Date.valueOf("1992-02-22"), 33, new ArrayList<>());
        employeeService.addEmployee(employee);
        assertEquals(employee, employeeService.findOldestEmployee());
    }

    @Test
    void shouldReturnSortedListByFirstName() {
        List<Employee> expected = employeeService.getAllEmployees();
        expected.sort(Comparator.comparing(Employee::getFirstName));
        List<Employee> actual = employeeService.getEmployeesOrderedByFirstName();
        assertArrayEquals(expected.toArray(), actual.toArray());
    }
}
