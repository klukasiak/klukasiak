package pl.edu.ug.projekt_spring;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import pl.edu.ug.projekt_spring.domain.Position;
import pl.edu.ug.projekt_spring.service.PositionService;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class PositionServiceTest {

    @Autowired
    PositionService positionService;

    @BeforeEach
    void setUp() {
        Position position = new Position(null, "A", "B", 222, new ArrayList<>());
        positionService.addPosition(position);
        position = new Position(null, "Bcc", "Cdddd", 3332, new ArrayList<>());
        positionService.addPosition(position);
    }

    @Test
    void shouldReturnAdded() {
        Position position = new Position(null, "Aads", "Bddd", 22222, new ArrayList<>());
        assertEquals(position, positionService.addPosition(position));
    }

    @Test
    void shouldReturnTwoPositions() {
        assertEquals(2, positionService.getAllPositions().size());
    }

    @Test
    void shouldReturnAddedById() {
        Position position = new Position(1L, "Aads", "Bddd", 22222, new ArrayList<>());
        positionService.addPosition(position);
        Position actual = positionService.getPositionById(1L);
        assertEquals(position, actual);
    }

    @Test
    void shouldReturnUpdated() {
        Position position = new Position(null, "Aads", "Bddd", 22222, new ArrayList<>());
        assertEquals(position, positionService.updatePosition(1L, position));
    }

    @Test
    void shouldReturnDeleted() {
        Position position = new Position(null, "Aads", "Bddd", 22222, new ArrayList<>());
        positionService.addPosition(position);
        assertEquals(position, positionService.deletePosition(position));
    }
}
