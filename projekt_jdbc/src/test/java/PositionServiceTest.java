import domain.Position;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import service.PositionService;
import service.PositionServiceJDBC;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PositionServiceTest {
    private PositionService ps;

    @BeforeEach
    void setUp() {
        Main.dbInitializer();
        ps = new PositionServiceJDBC();
    }

    @Test
    void shouldReturnInsertedPosition() {
        Position given = new Position("Example", "position", 222);
        Position actual = ps.insert(given);

        assertEquals(given, actual);
    }

    @Test
    void shouldReturnNullWhenInsertCalledWithNulls() {
        Position given = new Position(null, null, 0);
        Position actual = ps.insert(given);

        assertNull(actual);
    }

    @Test
    void shouldReturnExpectedList() {
        Position position = new Position("Example", "Position", 222);
        Position position2 = new Position("New", "Example", 333);

        ps.insert(position);
        ps.insert(position2);

        List<Position> result = ps.getAll();
        List<Position> expected = new ArrayList<>();

        expected.add(position);
        expected.add(position2);

        for (int i = 0; i < result.size(); i++) {
            assertEquals(expected.get(i), result.get(i));
        }
    }

    @Test
    void shouldReturnAddedPosition() {
        Position given = new Position("Example", "Position", 222);
        int index = ps.insert(given).getId();

        Position actual = ps.getById(index);

        assertEquals(given, actual);
    }

    @Test
    void shouldReturnNullPositionDoesntExists() {
        Position actual = ps.getById(1);
        assertNull(actual);
    }

    @Test
    void shouldReturnFindedByTitle() {
        Position given = new Position("Example", "Position", 222);
        ps.insert(given);
        Position actual = ps.getByTitle(given.getTitle());

        assertEquals(given, actual);
    }

    @Test
    void shouldReturnTrueWhenDeletePosition() {
        Position given = new Position("Example", "Position", 222);
        int index = ps.insert(given).getId();

        assertTrue(ps.deleteById(index));
    }

    @Test
    void shouldReturnUpdatedPosition() {
        Position given = new Position("Example", "Position", 222);
        int index = ps.insert(given).getId();
        given.setId(index);
        given.setDescription("Edited position");

        assertEquals(given, ps.update(index, given));
    }
}
