import domain.Employee;
import domain.Position;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import service.EmployeeService;
import service.EmployeeServiceJDBC;
import service.PositionService;
import service.PositionServiceJDBC;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeServiceTest {
    private EmployeeService es;
    private PositionService ps;

    @BeforeEach
    void setUp() {
        Main.dbInitializer();
        es = new EmployeeServiceJDBC();
        ps = new PositionServiceJDBC();
        ps.insert(new Position("Example", "Desc", 2000));
        ps.insert(new Position("Example2", "Desc2", 3000));
    }

    @Test
    void shouldReturnInsertedEmployee() {
        Employee given = new Employee("Abc", "Bca", Date.valueOf("2010-02-02"), 22, ps.getByTitle("Example"));
        Employee actual = es.insert(given);

        assertEquals(given, actual);
    }

    @Test
    void shouldReturnNullWhenInsertCalledWithNulls() {
        Employee given = new Employee(null, null, null, 0, null);
        Employee actual = es.insert(given);

        assertNull(actual);
    }

    @Test
    void shouldReturnExpectedList() {
        Employee employee = new Employee("Abc", "Bca", Date.valueOf("2010-02-02"), 22, ps.getByTitle("Example"));
        Employee employee2 = new Employee("Avvvvvbc", "Dca", Date.valueOf("2011-02-02"), 20, ps.getByTitle("Example2"));

        es.insert(employee);
        es.insert(employee2);

        List<Employee> result = es.getAll();
        List<Employee> expected = new ArrayList<>();

        expected.add(employee);
        expected.add(employee2);

        for (int i = 0; i < result.size(); i++) {
            assertEquals(expected.get(i), result.get(i));
        }
    }

    @Test
    void shouldReturnAddedEmployee() {
        Employee given = new Employee("Abc", "Bca", Date.valueOf("2010-02-02"), 22, ps.getByTitle("Example"));
        int index = es.insert(given).getId();

        Employee actual = es.getById(index);

        assertEquals(given, actual);
    }

    @Test
    void shouldReturnNullEmployeeDoesntExists() {
        Employee actual = es.getById(1);
        assertNull(actual);
    }

    @Test
    void shouldReturnTrueWhenDeleteEmployee() {
        Employee given = new Employee("Abc", "Bca", Date.valueOf("2010-02-02"), 22, ps.getByTitle("Example"));
        int index = es.insert(given).getId();

        assertTrue(es.deleteById(index));
    }

    @Test
    void shouldReturnUpdatedPosition() {
        Employee given = new Employee("Abc", "Bca", Date.valueOf("2010-02-02"), 22, ps.getByTitle("Example"));
        int index = es.insert(given).getId();
        given.setId(index);
        given.setLastName("Lastname");

        assertEquals(given, es.update(index, given));
    }
}
