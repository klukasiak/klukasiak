package domain;

import java.sql.Date;
import java.util.Objects;

public class Employee {
    private int id;
    private String firstName;
    private String lastName;
    private Date dateOfEmployement;
    private int age;
    private Position position;

    public Employee(String firstName, String lastName, Date dateOfEmployement, int age, Position position) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfEmployement = dateOfEmployement;
        this.age = age;
        this.position = position;
    }

    public Employee() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getDateOfEmployement() {
        return dateOfEmployement;
    }

    public void setDateOfEmployement(Date dateOfEmployement) {
        this.dateOfEmployement = dateOfEmployement;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dateOfEmployement=" + dateOfEmployement +
                ", age=" + age +
                ", position=" + position +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return age == employee.age &&
                Objects.equals(firstName, employee.firstName) &&
                Objects.equals(lastName, employee.lastName) &&
                Objects.equals(dateOfEmployement, employee.dateOfEmployement) &&
                Objects.equals(position, employee.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, dateOfEmployement, age, position);
    }
}
