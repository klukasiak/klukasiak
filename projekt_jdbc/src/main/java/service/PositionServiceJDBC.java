package service;

import domain.Position;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PositionServiceJDBC implements PositionService {

    private final String DB_URL = "jdbc:hsqldb:hsql://localhost/workdb";
    private final String SQL_INSERT_POSITION = "INSERT INTO POSITION(\"TITLE\", \"DESCRIPTION\", \"SALARY\") VALUES(?, ?, ?)";
    private final String SQL_SELECT_ALL_POSITION = "SELECT * FROM POSITION";
    private final String SQL_SELECT_POSITION_BY_ID = "SELECT * FROM POSITION WHERE ID=?";
    private final String SQL_SELECT_POSITION_BY_TITLE = "SELECT * FROM POSITION WHERE TITLE=?";
    private final String SQL_DELETE_POSITION_BY_ID = "DELETE FROM POSITION WHERE ID=?";
    private final String SQL_UPDATE_POSITION_BY_ID = "UPDATE POSITION SET TITLE=?, DESCRIPTION=?, SALARY=? WHERE ID=?";
    private final String SQL_UPDATE_EMPLOYEE_WHEN_DELETE = "UPDATE EMPLOYEE SET EMPLOYEE_POSITION=NULL WHERE EMPLOYEE_POSITION=?";

    private Connection connection;

    private PreparedStatement insertPositionPST;
    private PreparedStatement getAllPositionPST;
    private PreparedStatement getPositionByIdPST;
    private PreparedStatement getPositionByTitlePST;
    private PreparedStatement deletePositionByIdPST;
    private PreparedStatement updatePositionByIdPST;
    private PreparedStatement updateEmployeeWhenDeletePositionPST;

    public PositionServiceJDBC() {
        try {
            connection = DriverManager.getConnection(DB_URL);
            insertPositionPST = connection.prepareStatement(SQL_INSERT_POSITION, Statement.RETURN_GENERATED_KEYS);
            getAllPositionPST = connection.prepareStatement(SQL_SELECT_ALL_POSITION);
            getPositionByIdPST = connection.prepareStatement(SQL_SELECT_POSITION_BY_ID);
            getPositionByTitlePST = connection.prepareStatement(SQL_SELECT_POSITION_BY_TITLE);
            deletePositionByIdPST = connection.prepareStatement(SQL_DELETE_POSITION_BY_ID);
            updatePositionByIdPST = connection.prepareStatement(SQL_UPDATE_POSITION_BY_ID);
            updateEmployeeWhenDeletePositionPST = connection.prepareStatement(SQL_UPDATE_EMPLOYEE_WHEN_DELETE);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Position insert(Position position) {
        try {
            insertPositionPST.setString(1, position.getTitle());
            insertPositionPST.setString(2, position.getDescription());
            insertPositionPST.setInt(3, position.getSalary());
            insertPositionPST.execute();
            ResultSet generatedKey = insertPositionPST.getGeneratedKeys();
            generatedKey.next();
            position.setId(generatedKey.getInt(1));
            return position;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Position> getAll() {
        ArrayList<Position> allPositions = new ArrayList<>();

        try {
            ResultSet rs = getAllPositionPST.executeQuery();
            while (rs.next()) {
                allPositions.add(createPosition(rs));
            }
            return allPositions;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Position getById(int id) {
        try {
            getPositionByIdPST.setInt(1, id);
            ResultSet rs = getPositionByIdPST.executeQuery();
            rs.next();
            return createPosition(rs);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Position getByTitle(String title) {
        try {
            getPositionByTitlePST.setString(1, title);
            ResultSet rs = getPositionByTitlePST.executeQuery();
            rs.next();
            return createPosition(rs);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean deleteById(int id) {
        try {
            connection.setAutoCommit(false);
            updateEmployeeWhenDeletePositionPST.setInt(1, id);
            updateEmployeeWhenDeletePositionPST.execute();
            deletePositionByIdPST.setInt(1, id);
            deletePositionByIdPST.execute();
            connection.commit();
            connection.setAutoCommit(true);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            try {
                connection.rollback();
                connection.setAutoCommit(true);
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
            return false;
        }
    }

    public Position update(int id, Position position) {
        position.setId(id);
        try {
            updatePositionByIdPST.setString(1, position.getTitle());
            updatePositionByIdPST.setString(2, position.getDescription());
            updatePositionByIdPST.setInt(3, position.getSalary());
            updatePositionByIdPST.setInt(4, position.getId());
            updatePositionByIdPST.execute();
            return position;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Position createPosition(ResultSet rs) {
        try {
            Position position = new Position();
            position.setId(rs.getInt("ID"));
            position.setTitle(rs.getString("TITLE"));
            position.setDescription(rs.getString("DESCRIPTION"));
            position.setSalary(rs.getInt("SALARY"));
            return position;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
