package service;

import domain.Employee;
import domain.Position;

import java.util.List;

public interface EmployeeService {
    Employee insert(Employee employee);
    List<Employee> getAll();
    Employee getById(int id);
    List<Employee> getByPosition(Position position);
    boolean deleteById(int id);
    Employee update(int id, Employee employee);
}
