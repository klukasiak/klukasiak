package service;

import domain.Position;

import java.util.List;

public interface PositionService {
    Position insert(Position position);
    List<Position> getAll();
    Position getById(int id);
    Position getByTitle(String title);
    boolean deleteById(int id);
    Position update(int id, Position position);
}
