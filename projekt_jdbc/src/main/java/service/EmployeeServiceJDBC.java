package service;

import domain.Employee;
import domain.Position;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class EmployeeServiceJDBC implements EmployeeService{

    private final String DB_URL = "jdbc:hsqldb:hsql://localhost/workdb";
    private final String SQL_INSERT_EMPLOYEE = "INSERT INTO EMPLOYEE (\"FIRSTNAME\", \"LASTNAME\", \"DATEOFEMPLOYMENT\", \"AGE\", \"EMPLOYEE_POSITION\" )VALUES (?,?,?,?,?)";
    private final String SQL_SELECT_ALL_EMPLOYEE = "SELECT * FROM EMPLOYEE";
    private final String SQL_SELECT_EMPLOYEE_BY_ID = "SELECT * FROM EMPLOYEE WHERE ID=?";
    private final String SQL_SELECT_EMPLOYEE_BY_POSITION = "SELECT * FROM EMPLOYEE INNER JOIN POSITION ON POSITION.ID=EMPLOYEE.EMPLOYEE_POSITION WHERE TITLE=?";
    private final String SQL_DELETE_EMPLOYEE_BY_ID = "DELETE FROM EMPLOYEE WHERE ID=?";
    private final String SQL_UPDATE_EMPLOYEE_BY_ID = "UPDATE EMPLOYEE SET FIRSTNAME=?, LASTNAME=?, DATEOFEMPLOYMENT=?, AGE=?, EMPLOYEE_POSITION=? WHERE ID=?";

    private Connection connection;

    private PreparedStatement insertEmployeePST;
    private PreparedStatement getAllEmployeePST;
    private PreparedStatement getEmployeeByIdPST;
    private PreparedStatement getEmployeeByPositionPST;
    private PreparedStatement deleteEmployeeByIdPST;
    private PreparedStatement updateEmployeeByIdPST;

    private PositionService ps;

    public EmployeeServiceJDBC(){
        try {
            connection = DriverManager.getConnection(DB_URL);
            ps = new PositionServiceJDBC();
            insertEmployeePST = connection.prepareStatement(SQL_INSERT_EMPLOYEE, Statement.RETURN_GENERATED_KEYS);
            getAllEmployeePST = connection.prepareStatement(SQL_SELECT_ALL_EMPLOYEE);
            getEmployeeByIdPST = connection.prepareStatement(SQL_SELECT_EMPLOYEE_BY_ID);
            getEmployeeByPositionPST = connection.prepareStatement(SQL_SELECT_EMPLOYEE_BY_POSITION);
            deleteEmployeeByIdPST = connection.prepareStatement(SQL_DELETE_EMPLOYEE_BY_ID);
            updateEmployeeByIdPST = connection.prepareStatement(SQL_UPDATE_EMPLOYEE_BY_ID);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Employee insert(Employee employee) {
        try {
            insertEmployeePST.setString(1, employee.getFirstName());
            insertEmployeePST.setString(2, employee.getLastName());
            insertEmployeePST.setDate(3, employee.getDateOfEmployement());
            insertEmployeePST.setInt(4, employee.getAge());
            insertEmployeePST.setInt(5, employee.getPosition().getId());
            insertEmployeePST.execute();
            ResultSet generatedKey = insertEmployeePST.getGeneratedKeys();
            generatedKey.next();
            employee.setId(generatedKey.getInt(1));
            return employee;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Employee> getAll() {
        List<Employee> employees = new ArrayList<>();

        try {
            ResultSet rs = getAllEmployeePST.executeQuery();
            while(rs.next()){
                employees.add(createEmployee(rs));
            }
            return employees;
        } catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Employee getById(int id) {
        try {
            getEmployeeByIdPST.setInt(1, id);
            ResultSet rs = getEmployeeByIdPST.executeQuery();
            rs.next();
            return createEmployee(rs);
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Employee> getByPosition(Position position) {
        List<Employee> employees = new ArrayList<>();
        try {
            getEmployeeByPositionPST.setString(1, position.getTitle());
            ResultSet rs = getEmployeeByPositionPST.executeQuery();
            while(rs.next()){
                employees.add(createEmployee(rs));
            }
            return employees;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean deleteById(int id) {
        try {
            deleteEmployeeByIdPST.setInt(1, id);
            deleteEmployeeByIdPST.execute();
            return true;
        } catch (SQLException e){
            e.printStackTrace();
            return false;
        }
    }

    public Employee update(int id, Employee employee) {
        employee.setId(id);
        try {
            updateEmployeeByIdPST.setString(1, employee.getFirstName());
            updateEmployeeByIdPST.setString(2, employee.getLastName());
            updateEmployeeByIdPST.setDate(3, employee.getDateOfEmployement());
            updateEmployeeByIdPST.setInt(4, employee.getAge());
            updateEmployeeByIdPST.setInt(5, employee.getPosition().getId());
            updateEmployeeByIdPST.setInt(6, employee.getId());
            return employee;
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }

    private Employee createEmployee(ResultSet rs) {
        try {
            Employee employee = new Employee();
            employee.setId(rs.getInt("ID"));
            employee.setFirstName(rs.getString("FIRSTNAME"));
            employee.setLastName(rs.getString("LASTNAME"));
            employee.setDateOfEmployement(rs.getDate("DATEOFEMPLOYMENT"));
            employee.setAge(rs.getInt("AGE"));
            employee.setPosition(ps.getById(rs.getInt("EMPLOYEE_POSITION")));
            return employee;
        } catch(SQLException e){
            e.printStackTrace();
            return null;
        }
    }
}
